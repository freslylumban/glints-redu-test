//pake _ "underscore" untuk ke model lain

const storySchema = require("../models/Story").Story;
const userSchema = require("../models/User").User;
const { successResponse, errorResponse } = require("../helpers/response");

const multer = require("multer");
const cloudinary = require("cloudinary");
const datauri = require("datauri");

async function postStory(req, res) {
    
    var _userId = req.user._id

    let { title, body, category } = req.body
    let story = new storySchema({
      title,
      body,
      category
    })
    try{

        idUser = _userId
        await story.save()

        await storySchema.findOneAndUpdate(
          {_id: story._id},
          {$set: {_user: idUser}}
        )
        let result = await storySchema.findOne({_id: story._id})
                                      .populate({
                                        path: '_user',
                                        select: 'name'
                                      })

        res.status(201).json(successResponse(result));

    }
    catch(err){
        return res.status(408).json(errorResponse(err));
    }
}

async function get(req, res) {
  try {
    const user = await storySchema.paginate(
      {},
      { page: req.params.page, limit: 10, sort: "date" }
    );
    res.status(200).json(successResponse(user));
  }
  catch(err){
    res.status(400).json(errorResponse(err));
  }
}

async function getStoryById(req, res) {
  const user = await storySchema.findById(req.params.id).populate({
    path: "user",
    select: "name"
  });
  if(!user){
  	return res.status(404).json(errorResponse("There is no story"))
  }
  res.status(200).json(successResponse(user));

}

async function getStoryByUser(req, res) {
  const user = await storySchema.find({ _user: req.user._id }).populate({
    path: "_user",
    select: "name"
  });
  res.status(200).json(successResponse(user));
}

async function getStoryByCategory(req, res) {

  const stories = await storySchema.find({
    category: req.params.category
  });
  if(stories.length === 0) {
    return res.status(404).json(errorResponse("Sorry there is no story by this category"))
  }
  res.status(200).json(successResponse(stories));
}

async function updateStory(req, res) {
  var _userId = req.user._id;

  let beforeUpdated = await storySchema.findById(req.params.id);

  if (beforeUpdated._user._id != _userId) {
    return res.status(402).json(errorResponse("Sorry, it's not yours"));
  }
  await storySchema.findByIdAndUpdate(req.params.id, {
    $set: req.body
  });
  let afterUpdated = await storySchema.findById(req.params.id);
  res.status(200).json(successResponse(afterUpdated));
}

async function deleteStory(req, res) {
  var _userId = req.user._id;

    var _userId = req.user._id

    console.log(_userId)

    let beforeDeleted = await storySchema.findById(req.params.id)
    console.log(beforeDeleted._user)
    
    if(beforeDeleted._user != _userId){
        return res.status(422).json(errorResponse("Not Yours"))
    }
        await storySchema.findByIdAndDelete(req.params.id)
        let afterDeleted = await storySchema.findById(req.params.id)
        res.status(200).json(successResponse(afterDeleted, "Success Deleted"))
}

async function storyPict(req, res) {
  const uploader = multer().single("image");
  let fileUp = req.file;

  if (!fileUp) {
    return res.status(428).json({ message: "No file received" });
  } else {
    const dUri = new datauri();

    uploader(req, res, err => {
      let file = dUri.format(
        `${req.file.originalname}-${Date.now()}`,
        req.file.buffer
      );

      cloudinary.uploader.upload(file.content).then(data => {
        storySchema
          .findOneAndUpdate(
            { _id: req.params.id },
            { $set: { photo: data.secure_url } },
            { new: true }
          )
          .exec()
          .then(() => {
            return res
              .status(200)
              .json(successResponse(data.secure_url, "Picture uploaded"));
          });
      });
    });
  }
}

module.exports = {
  postStory,
  get,
  getStoryById,
  getStoryByUser,
  getStoryByCategory,
  updateStory,
  deleteStory,
  storyPict
};
