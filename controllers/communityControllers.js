const { Community } = require("../models/Community");
const { successResponse, errorResponse } = require("../helpers/response");
const multer = require("multer");
const cloudinary = require("cloudinary");
const datauri = require("datauri");

async function getAll(req, res) {
  const communities = await Community.find();
  res.status(200).json(successResponse(communities));
}

async function getById(req, res) {
  let community = await Community.findById(req.params.id);
  if (!community) {
    return res
      .status(404)
      .json(errorResponse("Invalid id, community not found"));
  }
  res.status(200).json(successResponse(community));
}

async function getByLocation(req, res) {
  let communities = await Community.find({ location: req.params.location });

  if (communities.length === 0) {
    return res
      .status(404)
      .json(errorResponse("there is no community in this location"));
  }
  res.status(200).json(successResponse(communities));
}

async function addCommunity(req, res) {
  try {
    let { name, url, description, location, photo } = req.body;
    let community = new Community({
      name,
      url,
      description,
      location,
      photo
    });

    await community.save();
    res.status(201).json(successResponse(community));
  } catch (err) {
    res.status(400).json(errorResponse(err.message));
  }
}

async function updateCommunity(req, res) {
  let community = await Community.findByIdAndUpdate(
    req.params.id,
    {
      $set: req.body
    },
    { new: true }
  );
  if (!community) {
    return res
      .status(404)
      .json(errorResponse("Invalid id, community not found"));
  }
  res.status(200).json(successResponse(community, "Updated"));
}

async function deleteCommunity(req, res) {
  let community = await Community.findByIdAndRemove(req.params.id);
  if (!community) {
    return res
      .status(404)
      .json(errorResponse("Invalid id, community not found"));
  }
  res.status(200).json(successResponse(community, "Community deleted"));
}

async function communityPict(req, res) {
  const uploader = multer().single("image");
  let fileUp = req.file;

  if (!fileUp) {
    return res.status(428).json(errorResponse("No file received"));
  } else {
    const dUri = new datauri();

    uploader(req, res, err => {
      let file = dUri.format(
        `${req.file.originalname}-${Date.now()}`,
        req.file.buffer
      );

      cloudinary.uploader
        .upload(file.content)
        .then(data => {
          Community.findOneAndUpdate(
            { _id: req.params.id },
            { $set: { photo: data } },
            { new: true }
          )
            .exec()
            .then(() => {
              return res
                .status(200)
                .json(successResponse(data.secure_url, "Picture uploaded"));
            });
        })
        .catch(err => {
          res.status(415).json(errorResponse(err.message));
        });
    });
  }
}

module.exports = {
  getAll,
  getById,
  getByLocation,
  addCommunity,
  updateCommunity,
  deleteCommunity,
  communityPict
};
