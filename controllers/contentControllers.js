const { Content } = require("../models/Content");
const { Comment } = require("../models/Comment");
const { successResponse, errorResponse } = require("../helpers/response");
const multer = require("multer");
const cloudinary = require("cloudinary");
const datauri = require("datauri");

async function getAll(req, res) {
  const contents = await Content.find();
  res.status(200).json(successResponse(contents));
}

async function get(req, res) {
  try {
    const contents = await Content.paginate(
      {},
      { page: req.params.page, limit: 10, sort: "date" }
    );
    res.status(200).json(successResponse(contents));
  } catch (err) {
    res.status(400).json(errorResponse(err));
  }
}

async function getById(req, res) {
  const content = await Content.findById(req.params.id);
  let comment = await Comment.find({ _content: req.params.id })
    .populate({
      path: "_user",
      select: "name photo"
    })
    .select("text date");
  if (!content) {
    return res.status(404).json(errorResponse("Invalid Id, content not found"));
  }
  res.status(200).json({ content: content, comment: comment });
}

async function getMyContent(req, res) {
  let contents = await Content.find({ _user: req.user._id });
  if (contents.length === 0) {
    return res.status(404).json(errorResponse("There is no content"));
  }

  res.status(200).json(successResponse(contents));
}

async function getByCategory(req, res) {
  const contents = await Content.find({
    category: req.params.category
  });
  if (contents.length === 0) {
    return res
      .status(404)
      .json(errorResponse("Sorry no content for this category"));
  }
  res.status(200).json(successResponse(contents));
}

async function addContent(req, res) {
  try {
    const { title, body, date, category, photo } = req.body;
    const content = new Content({
      title,
      body,
      category,
      date,
      photo,
      _user: req.user._id
    });

    await content.save();
    res.status(201).json(successResponse(content));
  } catch (err) {
    res.status(400).json(errorResponse(err.message));
  }
}

async function contentPict(req, res) {
  const uploader = multer().single("image");
  let fileUp = req.file;

  if (!fileUp) {
    return res.status(428).json(errorResponse("No file received"));
  } else {
    const dUri = new datauri();

    uploader(req, res, err => {
      let file = dUri.format(
        `${req.file.originalname}-${Date.now()}`,
        req.file.buffer
      );

      cloudinary.uploader
        .upload(file.content)
        .then(data => {
          Content.findOneAndUpdate(
            { _id: req.params.id },
            { $set: { photo: data } },
            { new: true }
          )
            .exec()
            .then(() => {
              return res
                .status(200)
                .json(successResponse(data.secure_url, "Picture uploaded"));
            });
        })
        .catch(err => {
          res.status(415).json(errorResponse(err.message));
        });
    });
  }
}

async function updateContent(req, res) {
  let content = await Content.findOneAndUpdate(
    { _id: req.params.id, _user: req.user._id },
    {
      $set: req.body
    },
    { new: true }
  );
  if (!content) {
    return res.status(404).json(errorResponse("Invalid id, content not found"));
  }
  res.status(200).json(successResponse(content, "Content updated"));
}

async function deleteContent(req, res) {
  let content = await Content.findOneAndRemove({
    _id: req.params.id,
    _user: req.user._id
  });
  if (!content) {
    return res.status(404).json(errorResponse("Invalid id, content not found"));
  }
  res.status(200).json(successResponse(content, "content deleted"));
}

async function getComment(req, res) {
  try {
    let comments = await Comment.find({
      _content: req.params.content
    }).populate("_user");
    res.status(200).json(successResponse(comments));
  } catch (err) {
    res.status(400).json(errorResponse(err.message));
  }
}

async function addComment(req, res) {
  try {
    let comment = new Comment({
      _user: req.user._id,
      _content: req.params.id,
      text: req.body.text
    });
    await comment.save();
    res.status(200).json(successResponse(comment));
  } catch (err) {
    res.status(400).json(errorResponse(err.message));
  }
}

async function delComment(req, res) {
  try {
    let comment = await Comment.findByIdAndRemove(req.params.id);
    res.status(200).json(successResponse(comment, "Comment deleted"));
  } catch (err) {
    res.status(400).json(errorResponse(err.message));
  }
}

module.exports = {
  get,
  getAll,
  addContent,
  getById,
  getMyContent,
  getByCategory,
  contentPict,
  updateContent,
  deleteContent,
  addComment,
  delComment,
  getComment
};
