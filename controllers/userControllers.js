const { User } = require("../models/User");
const { successResponse, errorResponse } = require("../helpers/response");
const bcrypt = require("bcryptjs");
const multer = require("multer");
const cloudinary = require("cloudinary");
const datauri = require("datauri");

async function get(req, res) {
  const user = await User.find().select("-password");
  res.status(200).json(successResponse(user));
}

async function getById(req, res) {
  const user = await User.findById(req.params.id).select("-password");
  if (!user) {
    return res.status(404).json(errorResponse("Invalid id, user not found"));
  }
  res.status(200).json(successResponse(user));
}

async function getCurrentUser(req, res) {
  let user = await User.findById(req.user._id).select("-password");

  /* istanbul ignore next */
  if (!user) {
    return res.status(404).json(errorResponse("Invalid User"));
  }
  res.status(200).json(successResponse(user));
}

async function add(req, res) {
  let { name, email, password, gender, interest, about } = req.body;
  let user = await User.findOne({ email: email });
  if (user) {
    return res.status(409).json(errorResponse("Email already registerd"));
  }
  user = new User({
    name,
    email,
    password,
    gender,
    about,
    interest
  });
  try {
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
    await user.save();
    res.status(201).json(successResponse(user));
  } catch (err) {
    return res.status(400).json(errorResponse(err.message));
  }
}

async function update(req, res) {
  let user = await User.findByIdAndUpdate(
    req.user._id,
    { $set: req.body },
    { new: true }
  );
  /* istanbul ignore next */
  if (!user) {
    return res.status(404).json(errorResponse("Invalid User"));
  }
  res.status(200).json({ data: user });
}

async function login(req, res) {
  let { email, password } = req.body;

  if (!email || email === "" || !password || password === "") {
    return res
      .status(400)
      .json(errorResponse("Email and Password are required"));
  }
  let user = await User.findOne({ email: email });
  if (!user) {
    return res.status(400).json(errorResponse("Wrong email"));
  }

  let validPass = await bcrypt.compare(password, user.password);
  if (!validPass) {
    return res.status(400).json(errorResponse("Wrong Password"));
  }

  const token = user.generateToken();
  res.status(200).json(successResponse(token));
}

async function userPict(req, res) {
  const uploader = multer().single("image");
  let fileUp = req.file;

  if (!fileUp) {
    return res.status(428).json(errorResponse("No file received"));
  } else {
    const dUri = new datauri();

    uploader(req, res, err => {
      let file = dUri.format(
        `${req.file.originalname}-${Date.now()}`,
        req.file.buffer
      );

      cloudinary.uploader
        .upload(file.content)
        .then(data => {
          User.findOneAndUpdate(
            { _id: req.user._id },
            { $set: { photo: data } },
            { new: true }
          )
            .exec()
            .then(() => {
              return res
                .status(200)
                .json({ message: "Picture uploaded", data: data.secure_url });
            });
        })
        .catch(err => {
          res.status(415).json(errorResponse(err.message));
        });
    });
  }
}

async function del(req, res) {
  let user = await User.findByIdAndRemove(req.params.id);
  /* istanbul ignore next */
  if (!user) {
    return res.status(404).json(errorResponse("Invalid User"));
  }

  res.status(200).json(successResponse(user, "Deleted"));
}

module.exports = {
  get,
  add,
  userPict,
  login,
  getById,
  getCurrentUser,
  update,
  del
};
