const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const router = require("./routers");
const cors = require("cors");
const port = process.env.PORT || 5000;

//DB
require("./config/db")();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.json());
app.use("/api", router);
app.get("/", (req, res) => {
  res.status(200).json({ message: "hello hello" });
});

app.listen(port, () => console.log(`connected to ${port}`));

module.exports = app;
