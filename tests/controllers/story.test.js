process.env.NODE_ENV = "test";

const mongoose = require("mongoose");
const { Community } = require("../../models/Community");
const { Story } = require("../../models/Story");
const { User } = require("../../models/User");

const bcrypt = require("bcryptjs");
const chai = require("chai");
const chaihttp = require("chai-http");
const server = require("../../index");
const should = chai.should();
const faker = require("faker");
const fs = require("fs");

chai.use(chaihttp);

let fakeAdmin = {
  name: "fake",
  email: "fake@mail.com",
  password: "fake"
};

let fakeAdmin2 = {
  name: "fake2",
  email: "fake2@mail.com",
  password: "fake2"
};

let fakeStory = {
  title: "test comm",
  body: "test url",
  category: "testIt"
};
let fakeId = "5d8129d830374d0016769548";

function login() {
  let user = new User(fakeAdmin);
  user.save();
  let token = user.generateToken();
  let bearer = `bearer ${token}`;
  let _user = user._id;
  return { bear: bearer, id: _user };
}

function login2() {
  let user = new User(fakeAdmin2);
  user.save();
  let token = user.generateToken();
  let bearer = `bearer ${token}`;
  let _user = user._id;
  return { bear: bearer, id: _user };
}

//PARENT BLOCK
//delete all table before test
describe("Story", () => {
  beforeEach(done => {
    Story.deleteMany({}, { new: true }, err => {
      User.deleteMany({}, { new: true }, err => {
        done();
      });
    });
  });
  let savestory
  
  describe("/POST story", () => {
    it("it should add a new story to db", done => {
      let usr = login();
      chai
        .request(server)
        .post("/api/stories/")
        .set("Authorization", usr.bear)
        .send(fakeStory)
        .end((err, res) => {
          res.should.have.status(201);
          savestory = res.body.data
          done();
        });
    });
  });

  describe("/POST story", () => {
    it("it should return error if story required fields is null", done => {
      let usr = login();
      chai
        .request(server)
        .post("/api/stories/")
        .set("Authorization", usr.bear)
        .send({
          title: "test comm",
          category: ["testIt"]
        })
        .end((err, res) => {
          res.should.have.status(408);
          done();
        });
    });
  });

  describe("/GET all Stories", () => {
    it("It should get all stories in db", done => {
      chai
        .request(server)
        .get("/api/stories/page/1/")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an("object");
          done();
        });
    });
  });

  describe("/GET all Stories", () => {
    it("It should fail to get all stories if page in 0", done => {
      chai
        .request(server)
        .get("/api/stories/page/0/")
        .end((err, res) => {
          res.should.have.status(400);
          res.body.should.be.an("object");
          done();
        });
    });
  });

  describe("/GET story by id", () => {
    it("it should return story by the given id", done => {
      let story = new Story(fakeStory);
      story.save((err, res) => {
        chai
          .request(server)
          .get("/api/stories/detail/" + story._id)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/GET story by id", () => {
    it("it should return 404 if story is not found", done => {
      let story = new Community(fakeStory);
      story.save((err, res) => {
        chai
          .request(server)
          .get("/api/stories/detail/" + fakeId)
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/GET Stories By Category", () => {
    it("it should return find stories by category", done => {
      let story = new Story(fakeStory);
      story.save((err, res) => {
        chai
          .request(server)
          .get("/api/stories/filter/" + story.category[0])
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/GET Stories By Category", () => {
    it("it should fail to return find category by nothing categories", done => {
      let story = new Story(fakeStory);
      story.save((err, res) => {
        chai
          .request(server)
          .get("/api/stories/filter/a")
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });
  
  describe("/GET Stories By User", () => {
    it("it should return find stories by user", done => {
      let story = new Story(fakeStory);
      let usr = login();
      story.save((err, res) => {
        chai
          .request(server)
          .get("/api/stories/mine/")
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT Story to Update", () => {
    it("it should update a story", done => {
      let usr = login();
      let story = new Story({
        _user: usr.id,
        title: "a",
        body: "b"
      });
      story.save((err, res) => {
        chai
          .request(server)
          .put("/api/stories/update/" + story._id)
          .set("Authorization", usr.bear)
          .send(fakeStory)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          })
        });
      });
  });

  describe("/PUT Story to fail Update", () => {
    it("it should fail to update a story because it's not this account's", done => {
      let usr = login();
      let otherUsr = login2();
      let story = new Story({
        _user: usr.id,
        title: 'a',
        body: 'b'
      })
      story.save((err, res) => {
        chai
          .request(server)
          .put("/api/stories/update/" + story._id)
          .set("Authorization", otherUsr.bear)
          .send(fakeStory)
          .end((err, res) => {
            res.should.have.status(402);
            done();
          });
      });
    });
});


  describe("/DELETE Story to Delete", () => {
    it("it should delete a story", done => {
      let usr = login();
      let story = new Story({
        _user: usr.id,
        title: 'a',
        body: 'b'
      })
      story.save((err, res) => {
        chai
          .request(server)
          .delete("/api/stories/delete/" + story._id)
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
});

 describe("/DELETE Story to Delete", () => {
    it("it should fail to delete a story because it's not this account's", done => {
      let usr = login();
      let otherUsr = login2();
      let story = new Story({
        _user: usr.id,
        title: 'a',
        body: 'b'
      })
      story.save((err, res) => {
        chai
          .request(server)
          .delete("/api/stories/delete/" + story._id)
          .set("Authorization", otherUsr.bear)
          .end((err, res) => {
            res.should.have.status(422);
            done();
          });
      });
    });
});

  describe("/PUT picture", () => {
    it("it should update story picture", done => {
      let usr = login();
      let story = new Story(fakeStory);
      let file = "./public/images/400-badRequest.png";
      story.save((err, res) => {
        chai
          .request(server)
          .put("/api/stories/pict/" + story._id)
          .set("Authorization", usr.bear)
          .attach("file", fs.readFileSync(`${file}`), "icon.png")
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should error update story picture because no file input", done => {
      let usr = login();
      let story = new Story(fakeStory);
      let file = "./public/images/";
      story.save((err, res) => {
        chai
          .request(server)
          .put("/api/stories/pict/" + story._id)
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(428);
            done();
          });
      });
    });
});
});
