process.env.NODE_ENV = "test";

const { User } = require("../../models/User");

const bcrypt = require("bcryptjs");
const chai = require("chai");
const chaihttp = require("chai-http");
const server = require("../../index");
const fs = require("fs");
const should = chai.should();

chai.use(chaihttp);

let fakeAdmin = {
  name: "fake",
  email: "fake@mail.com",
  password: "fake",
  isAdmin: true,
  photo: {
    public_id: "ysz6cc1nqyis8os8nvth",
    secure_url:
      "https://res.cloudinary.com/sunaryaagung/image/upload/v1569573871/ysz6cc1nqyis8os8nvth.jpg"
  }
};

function login() {
  let user = new User(fakeAdmin);
  let token = user.generateToken();
  let bearer = `bearer ${token}`;
  user.save();
  let _user = user._id;
  return { bear: bearer, id: _user };
}

let fakeId = "5d8129d830374d0016769548";

describe("User", () => {
  beforeEach(done => {
    User.deleteMany({}, { new: true }, err => {
      done();
    });
  });

  afterEach(done => {
    User.deleteMany({}, { new: true }, err => {
      done();
    });
  });

  describe("/GET User", () => {
    it("it should return a list of user", done => {
      chai
        .request(server)
        .get("/api/users")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe("/GET User By Id", () => {
    it("it should return a list of user", done => {
      let usr = new User(fakeAdmin);
      usr.save((err, res) => {
        chai
          .request(server)
          .get("/api/users/detail/" + usr._id)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/GET User By Id", () => {
    it("it should return error 404 if user is not found", done => {
      chai
        .request(server)
        .get("/api/users/detail/" + fakeId)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe("/POST Add User", () => {
    it("should add a new user to db", done => {
      chai
        .request(server)
        .post("/api/users")
        .send(fakeAdmin)
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });
  });

  describe("/POST Add User", () => {
    it("should return error 409 if user alrd registered", done => {
      let usr = new User(fakeAdmin);
      usr.save((err, res) => {
        chai
          .request(server)
          .post("/api/users")
          .send(fakeAdmin)
          .end((err, res) => {
            res.should.have.status(409);
            done();
          });
      });
    });
  });

  describe("/POST Add User", () => {
    it("should return error 400 req body is bad", done => {
      chai
        .request(server)
        .post("/api/users")
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/POST LOGIN USER", () => {
    it("it should return ok if user login with the right data", done => {
      let salt = bcrypt.genSaltSync(10);
      let usr = new User(fakeAdmin);
      usr.password = bcrypt.hashSync(usr.password, salt);
      usr.save((err, res) => {
        chai
          .request(server)
          .post("/api/users/login")
          .send({ email: fakeAdmin.email, password: fakeAdmin.password })
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/POST LOGIN", () => {
    it("it should return error 400 if no email or password", done => {
      chai
        .request(server)
        .post("/api/users/login")
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/POST LOGIN", () => {
    it("it should return error 400 if wrong email ", done => {
      chai
        .request(server)
        .post("/api/users/login")
        .send({ email: "a", password: "b" })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/POST LOGIN USER", () => {
    it("it should return error 400 when user email is wrong", done => {
      let salt = bcrypt.genSaltSync(10);
      let usr = new User(fakeAdmin);
      usr.password = bcrypt.hashSync(usr.password, salt);
      usr.save((err, res) => {
        chai
          .request(server)
          .post("/api/users/login")
          .send({ email: fakeAdmin.email, password: "a" })
          .end((err, res) => {
            res.should.have.status(400);
            done();
          });
      });
    });
  });

  describe("/Get current user", () => {
    it("it should return current login user", done => {
      let user = new User(fakeAdmin);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .get("/api/users/me")
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT user", () => {
    it("it should edit user in db", done => {
      let user = new User(fakeAdmin);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .put("/api/users/update")
          .set("Authorization", bearer)
          .send({ name: "edited" })
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/DELETE USER", () => {
    it("it should delete user from db", done => {
      let usr = new User(fakeAdmin);
      let token = `bearer ${usr.generateToken()}`;
      usr.save((err, res) => {
        chai
          .request(server)
          .delete("/api/users/del/" + usr._id)
          .set("Authorization", token)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/DELETE USER", () => {
    it("it should return 404 is user is not found", done => {
      let usr = login();
      chai
        .request(server)
        .delete("/api/users/del/" + fakeId)
        .set("Authorization", usr.bear)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe("/PUT picture", () => {
    it("it should update user picture", done => {
      let user = new User(fakeAdmin);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      let file = "/home/arya/binar/redu-mainan/tests/controllers/icon.png";
      user.save((err, res) => {
        chai
          .request(server)
          .put("/api/users/pict")
          .set("Authorization", bearer)
          .attach("file", fs.readFileSync(`${file}`), "icon.png")
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should return status 428 if there is no pict", done => {
      let user = new User(fakeAdmin);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      user.save((err, res) => {
        chai
          .request(server)
          .put("/api/users/pict")
          .set("Authorization", bearer)
          .end((err, res) => {
            res.should.have.status(428);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should return status 415 if file format is wrong", done => {
      let user = new User(fakeAdmin);
      let token = user.generateToken();
      let bearer = `bearer ${token}`;
      let bfile = "/home/arya/binar/redu-mainan/tests/controllers/user.test.js";
      user.save((err, res) => {
        chai
          .request(server)
          .put("/api/users/pict")
          .set("Authorization", bearer)
          .attach("file", fs.readFileSync(`${bfile}`), "index.js")
          .end((err, res) => {
            res.should.have.status(415);
            done();
          });
      });
    });
  });
});
