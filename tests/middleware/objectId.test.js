process.env.NODE_ENV = "test";

const { User } = require("../../models/User");

const chai = require("chai");
const chaihttp = require("chai-http");
const should = chai.should();
const server = require("../../index");
const fs = require("fs");

chai.use(chaihttp);

describe("MIDDLEWARE", () => {
  describe("OBJECT ID", () => {
    it("it should return err 400 if its not a valid object id", done => {
      chai
        .request(server)
        .get("/api/contents/detail/1")
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });
});
