process.env.NODE_ENV = "test";

const chai = require("chai");
const chaihttp = require("chai-http");
const should = chai.should();
const server = require("../../index");
const fs = require("fs");

chai.use(chaihttp);

describe("MIDDLEWARE", () => {
  describe("/AUTH", () => {
    it("it should return err 401 if there is no token", done => {
      chai
        .request(server)
        .get("/api/users/me")
        .end((err, res) => {
          res.should.have.status(401);
          done();
        });
    });
  });

  describe("/AUTH", () => {
    it("it should return err 422 if token is invalid", done => {
      chai
        .request(server)
        .get("/api/users/me")
        .set("Authorization", "a")
        .end((err, res) => {
          res.should.have.status(422);
          done();
        });
    });
  });
});
