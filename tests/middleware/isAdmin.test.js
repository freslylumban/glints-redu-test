process.env.NODE_ENV = "test";

const { User } = require("../../models/User");

const chai = require("chai");
const chaihttp = require("chai-http");
const should = chai.should();
const server = require("../../index");
const fs = require("fs");

chai.use(chaihttp);

describe("MIDDLEWARE", () => {
  describe("/ADMIN", () => {
    it("it should return err 403 if user is not admin", done => {
      let user = new User({
        name: "admin",
        password: "admin",
        email: "admin@mail.com"
      });
      let token = `bearer ${user.generateToken()}`;
      user.save((err, res) => {
        chai
          .request(server)
          .post("/api/contents")
          .set("Authorization", token)
          .send({ name: "a" })
          .end((err, res) => {
            res.should.have.status(403);
            done();
          });
      });
    });
  });
});
