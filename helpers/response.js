function successResponse(results, message) {
  return {
    success: true,
    message: message,
    data: results
  };
}

function errorResponse(message, err) {
  return {
    success: false,
    err: err,
    message: message
  };
}

module.exports = { successResponse, errorResponse };
