const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const objectId = require("../middleware/objectId");
const isAdmin = require("../middleware/isAdmin");
const cloudinaryConfig = require("../config/cloudinaryConfig");
const upload = require("../middleware/multer");
const { destroyContent } = require("../middleware/removePhoto");
const {
  get,
  getAll,
  getById,
  getMyContent,
  addContent,
  contentPict,
  updateContent,
  deleteContent,
  getByCategory,
  addComment,
  delComment,
  getComment
} = require("../controllers/contentControllers");

router.use("*", cloudinaryConfig);
router.get("/", getAll);
router.get("/page/:page", get);
router.get("/detail/:id", objectId, getById);
router.get("/filter/:category", getByCategory);
router.get("/mine", auth, isAdmin, getMyContent);

router.post("/", auth, isAdmin, addContent);

router.put(
  "/pict/:id",
  auth,
  objectId,
  isAdmin,
  upload.single("file"),
  contentPict
);
router.put("/update/:id", auth, objectId, isAdmin, updateContent);

router.delete(
  "/delete/:id",
  auth,
  objectId,
  isAdmin,
  destroyContent,
  deleteContent
);

router.get("/comment/:content", auth, getComment);
router.post("/comment/:id", auth, addComment);
router.delete("/comment/:id", auth, objectId, delComment);

module.exports = router;
