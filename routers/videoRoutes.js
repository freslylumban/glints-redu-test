const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const objectId = require("../middleware/objectId");
const {
  getAll,
  getById,
  getByCategory,
  getByUser,
  getMyVideo,
  addVideo,
  updateVideo,
  deleteVideo
} = require("../controllers/videoControllers");

router.get("/", getAll);
router.get("/detail/:id", objectId, getById);
router.get("/category", getByCategory);
router.get("/user", getByUser);
router.get("/mine", getMyVideo);
router.post("/", auth, addVideo);
router.put("/update/:id", auth, objectId, updateVideo);
router.delete("/delete/:id", auth, objectId, deleteVideo);

module.exports = router;
