const express = require("express");
const router = express.Router();
const objectId = require("../middleware/objectId");
const auth = require("../middleware/auth");
const isAdmin = require("../middleware/isAdmin");
const cloudinaryConfig = require("../config/cloudinaryConfig");
const upload = require("../middleware/multer");
const { destroyCommunity } = require("../middleware/removePhoto");
const {
  getAll,
  getById,
  getByLocation,
  addCommunity,
  updateCommunity,
  communityPict,
  deleteCommunity
} = require("../controllers/communityControllers");

router.use("*", cloudinaryConfig);

router.get("/", getAll);
router.get("/detail/:id", objectId, getById);
router.get("/filter/:location", getByLocation);
router.post("/", auth, isAdmin, addCommunity);
router.put(
  "/pict/:id",
  auth,
  isAdmin,
  objectId,
  upload.single("file"),
  communityPict
);
router.put("/update/:id", auth, isAdmin, objectId, updateCommunity);
router.delete(
  "/delete/:id",
  auth,
  isAdmin,
  objectId,
  destroyCommunity,
  deleteCommunity
);

module.exports = router;
