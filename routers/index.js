const express = require("express");
const router = express.Router();
const user = require("./userRoutes");
const story = require("./storyRoutes");
const content = require("./contentRoutes");
const community = require("./communityRoutes");
const video = require("./videoRoutes");

router.use("/users", user);
router.use("/stories", story);
router.use("/contents", content);
router.use("/communities", community);
router.use("/videos", video);

module.exports = router;
