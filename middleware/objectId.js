const mongoose = require("mongoose");
const { errorResponse } = require("../helpers/response");

module.exports = function(req, res, next) {
  if (!mongoose.Types.ObjectId.isValid(req.params.id))
    return res.status(400).json(errorResponse("Invalid Id"));
  next();
};
