const { User } = require("../models/User");
const { errorResponse } = require("../helpers/response");
const jwt = require("jsonwebtoken");

module.exports = async function(req, res, next) {
  let token = req.header("Authorization");
  if (!token) {
    return res.status(401).json(errorResponse("Access denied login pls"));
  }

  let tokenSplit = token.split(" ");

  try {
    let decoded = jwt.verify(tokenSplit[1], process.env.SECRET_KEY);
    req.user = decoded;
    let user = await User.findById(req.user._id);
    if (!user) {
      return res.status(404).json(errorResponse("Invalid User"));
    }
    next();
  } catch (err) {
    res.status(422).json(errorResponse("Invalid Token"));
  }
};
