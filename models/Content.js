const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const contentSchema = new mongoose.Schema({
  title: { type: String, required: true },
  body: { type: String, required: true },
  category: [String],
  _user: { type: mongoose.Schema.Types.ObjectId, ref: "users" },
  date: { type: Date, default: Date.now },
  photo: { public_id: String, secure_url: String }
});

contentSchema.plugin(mongoosePaginate);

const Content = new mongoose.model("contents", contentSchema);

exports.Content = Content;
