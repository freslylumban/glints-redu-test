const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");
require("mongoose-type-email");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const userSchema = new mongoose.Schema({
  name: { type: String, required: true },
  email: { type: mongoose.SchemaTypes.Email, required: true, unique: true },
  password: { type: String, required: true },
  gender: { type: String, enum: ["male", "female"] },
  about: { type: String },
  interest: [String],
  isAdmin: { type: Boolean, default: false },
  photo: { public_id: String, secure_url: String }
});

userSchema.plugin(uniqueValidator);

userSchema.methods.generateToken = function() {
  const token = jwt.sign({ _id: this._id }, process.env.SECRET_KEY);
  return token;
};

const User = new mongoose.model("users", userSchema);

exports.User = User;
