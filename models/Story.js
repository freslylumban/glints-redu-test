const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
require("dotenv").config();

const storySchema = new mongoose.Schema({
  _user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },
  title: { type: String, required: true },
  body: { type: String, required: true },
  date: { type: Date, default: Date.now() },
  category: [String],
  photo: { public_id: String, secure_url: String }
});

storySchema.plugin(mongoosePaginate);

const Story = new mongoose.model("story", storySchema);

module.exports = { Story };
