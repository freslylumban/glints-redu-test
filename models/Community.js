const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const communitySchema = new mongoose.Schema({
  name: { type: String, required: true, unique: true },
  url: { type: String, required: true },
  description: { type: String },
  location: { type: String, required: true },
  photo: { public_id: String, secure_url: String }
});

communitySchema.plugin(uniqueValidator);

const Community = new mongoose.model("communities", communitySchema);

exports.Community = Community;
